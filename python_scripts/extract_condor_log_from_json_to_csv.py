## @Author Teja Ram Seervi, teja.ram.seervi@cern.ch
## Purpose - Converts HTCondor JSON logs to CSV logs 

import simplejson as json
import os, logging
# pyspark liberary
from pyspark import SparkConf, SparkContext #HiveContext
from pyspark.sql import SQLContext

## list of fields to be extracted from the condor log
cols = 'JobPid, LastRemoteHost, User, JobStartDate, JobDuration, ExitCode, ExitStatus, ResidentSetSize, RequestMemory,\
 JobUniverse, RequestCpus, RemoteWallClockTime,  RemoteSysCpu, RemoteUserCpu, x509UserProxyVOName, MATCH_HEPSPEC, MATCH_TotalCpus'

def json_to_csv(sqlContext, input_dir, output_dir):
	'''
	Extract logs from input_dir and saves them in output_dir(text format)
	'''
	logging.critical('==================== Extracting log from: ' + input_dir +'=========================')

	# Read the JSON data of condor log into data frame df1
	# Reads a directory containing multiple JSON files 
	df1 = sqlContext.read.json(input_dir)
	
	# Register the dataframe
	df1.createOrReplaceTempView("condor")
	
	# Extract the required fields in another data farme df2
	df2 = sqlContext.sql('select '+cols+' from condor')

	# Save the df2 in csv format in hdfs
	df2.write.mode("overwrite").csv(output_dir)
	
	logging.critical('==================== Extracted log from: ' + input_dir +'=========================')

def usage():
	''' Displays how to run this program '''

	print(' --------------------------------------------------------------------------------------------------')
	print('example $spark-submit --master local condor_log_extract_day_wise.py')
	print("example $spark-submit condor_log_extract_day_wise.py")
	print(' --------------------------------------------------------------------------------------------------')
	sys.exit(' ')

if __name__ == '__main__':
	logging.basicConfig(level=logging.WARNING, format='%(asctime)s: %(message)s', datefmt='%H:%M:%S')

	## creteate spark and sql context
	conf = SparkConf()
	conf.setAppName('Condor Log Extraction')
	sc = SparkContext(conf = conf)
	sqlContext = SQLContext(sc)
	sqlContext.sql('set spark.sql.caseSensitive=true')
	logging.critical('Created SparkContext, SQLContext.')

        with open('config_condor_extract.json') as f:
                config = json.load(f)

	with open('missing_days.json') as f:
    		missing = json.load(f)

        for y in missing:
                yyyy = '{:04d}'.format(int(y))
                for m in missing[y]:
                        mm='{:02d}'.format(int(m))
			            # Extract log for each day
                        for d in missing[y][m]:
                                # Condor Log File Name Construction
                                dd = '{:02d}'.format(d)
				                if yyyy < 2018:
					                input_dir = 'hdfs:///user/accservice/batch/condor/' + yyyy + '/' + mm + '/' + dd + '/*'
				                else: # Log Data from Jan 2018 resides in cs3.cern.ch i.e. S3 server 
					                input_dir = config['hdfs_dir'] +'condor_json/'+ yyyy + '/' + mm + '/' + dd + '/'

				                output_dir = config['hdfs_dir'] + 'condor_csv/' + yyyy + mm + dd + '/'
 
				                logging.critical('input_dir: ' + input_dir)
				                logging.critical('output_dir: ' + output_dir)
				                json_to_csv(sqlContext, input_dir, output_dir)
