## @Author Teja Ram Seervi, tejas@barc.gov.in 
## Purpose:
## 1. Copy htcondor log from S3 to local machine
## 2. Uncompress the copied log
## 3. Copy uncompressed los to hdfs
## 4. Convert JSON to CSV format at hdfs
## 5. Copy CSV to local machine

import sys
from subprocess import Popen, PIPE
from datetime import datetime,timedelta
import simplejson as json
import os, logging

def find_missing_days(year,month,day):
        '''
	Returns a list of days for which condor log are not copied from S3 to eos 
	'''

        yyyy = '{:04d}'.format(year)
        mm = '{:02d}'.format(month)
        a = range(1,day)
	path = '%s%s/%s/' %(config["local_batch_condor_dir"],yyyy,mm)
	# On month/year change directory needs to be created
	if(not(os.path.exists(path))):
		os.system('mkdir -p %s' %path)

	b = map(int,os.listdir(path))
        diff = list(set(a) - set(b))

        return(diff)


def get_prev_month_last_day(dt):
        '''
	Returns last day of previous month
	'''

        dt1 = dt.replace(day=1)
        dt2 = dt1 - timedelta(days=1)
        return(dt2)


def build_config_for_missing_days():
        '''
	Builds configuration for missing condor log on eos
	'''

        today = datetime.today() - timedelta(days=0)
        prev_month_last_day = get_prev_month_last_day(today)

        #Build JSON Config for missing days
        missing = {}
        missing[today.year]={}
        missing[prev_month_last_day.year]={}
        missing[today.year][today.month] = find_missing_days(today.year,today.month,today.day)
        missing[prev_month_last_day.year][prev_month_last_day.month] = find_missing_days(prev_month_last_day.year,prev_month_last_day.month,prev_month_last_day.day+1)

        f=open('missing_days.json','w')
        f.write(json.dumps(missing))
        f.write('\n')
        f.close()

        return(missing)


def make_or_clean_hdfs_dir():
        '''
        1. Delete any old file present at hadoop file system, before copying the new ones
        2. Create hdfs directories for input(csv) and output(csv) if not present already
        '''

        hdfs_condor_json_dir = config['hdfs_dir'] + 'condor_json/'
        hdfs_condor_csv_dir = config['hdfs_dir'] + 'condor_csv/'
	
        if os.system('hdfs dfs -test -d %s' %hdfs_condor_json_dir):
                cmd = 'hdfs dfs -mkdir -p %s'% hdfs_condor_json_dir
		logging.critical(cmd)
                os.system(cmd)

        else:
                cmd = 'hdfs dfs -rm -r -skipTrash %s'%(hdfs_condor_json_dir + '*')
                logging.critical(cmd)
                os.system(cmd)
        if os.system('hdfs dfs -test -d %s' %hdfs_condor_csv_dir):
                cmd = 'hdfs dfs -mkdir -p %s'% hdfs_condor_csv_dir
		logging.critical(cmd)
                os.system(cmd)
        else:
                cmd = 'hdfs dfs -rm -r -skipTrash %s'%(hdfs_condor_csv_dir + '*')
		logging.critical(cmd)
                os.system(cmd)


def copy_files_from_s3_to_local(missing):
        '''
        copy from s3 to local file
        '''

	for y in missing:
		yyyy = '{:04d}'.format(y)
		for m in missing[y]:
			mm='{:02d}'.format(m)
			for d in missing[y][m]:
	                	# Condor Log File Name Construction
		                dd = '{:02d}'.format(d)
		
        		        #local_batch_condor_dst_dir = config["local_batch_condor_dir"] + yyyy +'/'+ mm +'/'
		                #s3_src_dir = config["s3_bucket_object"] + yyyy +'/'+ mm +'/'+  dd 
        		        #cmd = 's3cmd sync ' + s3_src_dir +' '+ local_batch_condor_dst_dir
	
                		s3_src_dir = config["s3_bucket_fs"] + yyyy +'/'+ mm +'/'+  dd
        		        local_batch_condor_dst_dir = config["root_eosuser_local_batch_condor_dir"] + yyyy +'/'+ mm +'/'+dd
        	        	cmd = 'hadoop distcp -Dfs.s3a.access.key=%s -Dfs.s3a.secret.key=%s -Dfs.s3a.endpoint=%s %s %s' %(config["s3_access_key"],config["s3_secret_key"],config["s3_endpoint"],s3_src_dir,local_batch_condor_dst_dir)
				logging.critical('Copying: %s to %s'%(s3_src_dir,local_batch_condor_dst_dir))
				process = Popen(cmd.split(),stdout=PIPE,stderr=PIPE)
				stdout, stderr = process.communicate()
                                if(stderr):
					logging.critical(stderr.strip())
					sys.exit(10)
		        	#os.system(cmd)


def copy_files_from_s3_to_hdfs(missing):
        '''
        copy from s3 to local file
        '''
	
        for y in missing:
                yyyy = '{:04d}'.format(y)
                for m in missing[y]:
                        mm='{:02d}'.format(m)
			hdfs_dst_dir = config['hdfs_dir']+'condor_json/'+ yyyy +'/'+ mm +'/'
                        for d in missing[y][m]:
                                # Condor Log File Name Construction
                                dd = '{:02d}'.format(d)
				hdfs_dst_dir = config['hdfs_dir']+'condor_json/'+ yyyy +'/'+ mm +'/'+dd
                		s3_src_dir = config["s3_bucket_fs"] + yyyy +'/'+ mm +'/'+  dd  

                		cmd = 'hadoop distcp -Dfs.s3a.access.key=%s -Dfs.s3a.secret.key=%s -Dfs.s3a.endpoint=%s %s %s' %(config["s3_access_key"],config["s3_secret_key"],config["s3_endpoint"],s3_src_dir,hdfs_dst_dir)
                                logging.critical('Copying: %s to %s'%(s3_src_dir,hdfs_dst_dir))
                                process = Popen(cmd.split(),stdout=PIPE,stderr=PIPE)
                                stdout, stderr = process.communicate()
				if(stderr):
                                        logging.critical(stderr.strip())
                                        sys.exit(11)
		                #os.system(cmd)


def copy_files_from_hdfs_to_local(missing):
        '''
        copy from hdfs to local file
        '''

        for y in missing:
                yyyy = '{:04d}'.format(y)
                for m in missing[y]:
                        mm='{:02d}'.format(m)
                        for d in missing[y][m]:
                                # Condor Log File Name Construction
                                dd = '{:02d}'.format(d)

		                # Merge all log data files of signle day 
                		# from extracted directory into a single file
		                hdfs_src_dir = config['hdfs_dir'] +'condor_csv/'+ yyyy + mm + dd+'/' 
                		local_dst_file = config['local_dir'] + yyyy + mm + dd + '.condorlog'
		                cmd = 'hdfs dfs -getmerge ' + hdfs_src_dir +' '+ local_dst_file
		                os.system(cmd)

		                # Add header line to merged data
                		local_header_file = config['local_dir'] + 'header.csv'
		                local_data_file = local_dst_file
		                local_csv_file = local_data_file+'.csv'
		                cmd = 'cat '+ local_header_file + ' ' +local_data_file + ' > ' + local_csv_file
		                os.system(cmd)

                		# Remove data file without header file
		                cmd = 'rm '+ local_data_file
		                os.system(cmd)


if __name__ == '__main__':
	logging.basicConfig(level=logging.WARNING, format='%(asctime)s: %(message)s', datefmt='%H:%M:%S')
	
	with open('/eos/project/i/it-awg/sajc/python_scripts/config_condor_extract.json') as f:
    		config = json.load(f)
	
    logging.critical('Stared: Building configuraion for missing logs')
	missing = build_config_for_missing_days()
    logging.critical('Completed: Building configuration for missing logs')

	logging.critical('Stared: Copying from S3 to local:json.gz')
    copy_files_from_s3_to_local(missing)
	logging.critical('Completed: Copying from S3 to local:json.gz')
	
    logging.critical('Started: Purging hdfs dirs')
    make_or_clean_hdfs_dir()
	logging.critical('Completed: Purging hdfs dirs')
	
	logging.critical('Started: Copying from s3 to hdfs:json.gz')
    copy_files_from_s3_to_hdfs(missing)
	logging.critical('Completed: Copying from s3 to hdfs:json.gz')

    logging.critical('Started: Sprak-submit job')
	cmd = 'spark-submit /eos/project/it-awg/sajc/python_scripts/extract_condor_log_from_json_to_csv.py'
	logging.critical(cmd)
	os.system(cmd)
    logging.critical('Completed: Spark-submit job')

    logging.critical('Started: Copying from hdfs to local:csv')
    copy_files_from_hdfs_to_local(missing)
    logging.critical('Completed: Copying ifrom hdfs to local:csv')
	
