#!/usr/bin/env python
import getopt, sys, gzip,re

def parse_line(l):
	'''
	Extract required fields from each line and return them comma separated format
	'''

	# Fields are seprated by sumbol &
    	words = l.split("&")

    	rc =[]
	'''
	Please have look for field detail at https://twiki.cern.ch/twiki/bin/view/ITAnalyticsWorkingGroup/EosFileAccessLogs

	td  = client trace identifier (<username>.<pid>@<client-host>)  = td(transaction descriptor): #cuser.cpid:fd@chost here c = client
	This identifier can be used to aggregate sessions (of the same user, with the same job on the same machine) 

	ots = File open time as unix timestamp (in seconds since January 1st, 1970 at UTC)

	cts = File close time as unix timestamp (in seconds since January 1st, 1970 at UTC) 

	rb  = Bytes read during file open (in Bytes) 

	wb  = Bytes written during file open (in Bytes) 
	'''
    	for w in words:
        	tokens = w.split("=", 1)
		if tokens[0] == 'td':
			matchObj = re.match( r'(.*)\.(.*):(.*)@(.*)', tokens[1], re.M|re.I)

			cuser = matchObj.group(1)
			cpid = matchObj.group(2)
			cfd = matchObj.group(3)
			chost = matchObj.group(4)
			
			rc.append(chost)
			rc.append(cpid)
			rc.append(cuser)	

        	elif tokens[0] in ("fid","ots","cts","rb","wb","osize","csize"): 
            		rc.append(tokens[1])
    	return ",".join(rc) + "\n"

def usage():
	''' Displays how to run this program '''

	print ' ---------------------------------------------------------'
    	print ' EosUnzipFilter.py --infile=<file> [--output_file=<file> ]'
    	print ' ---------------------------------------------------------'
    	sys.exit(' ')


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:],"h:ion",["help","infile=","outfile="])
    except getopt.GetoptError:
        usage()

    output_filename = ""
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
        if o in ("-i", "--infile"):
            input_file_name = str(a)
        if o in ("-o", "--outfile"):
            output_filename = str(a)

    input_file = gzip.open(input_file_name)

    if output_filename:
        output_file = open(output_filename,"w")
    else:
        output_file = sys.stdout

    # header line construction
    header_line  = "chost,cpid,cuser,fid,ots,cts,rb,wb,osize,csize\n"
    output_file.write(header_line)
    for line in input_file:
        output_file.write(parse_line(line))
    output_file.close()
    input_file.close()
