import os
rootdir = raw_input('Provide the directory:')
#rootdir = '/eos/project/i/it-awg/batch/condor/2018/05'
for root, subdirs, files in os.walk(rootdir):
	if files:
		print('-----------------Uncompressing at %s---------------------------' %root)
                depth = len(root.split('/'))
		for f in files:
			if f.endswith('.gz'):
				print("  " * depth+ f)
				cmd = 'gzip -d %s' %os.path.join(root,f)
				os.system(cmd)
