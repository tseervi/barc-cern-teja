## @Author gurmeet Singh, gurmeet.singh@cern.ch 
## Extract htcondor log of specified no. of days from hdfs and save in csv format in hdfs
## Copy extrated log from hdfs to local file system as a single csv file for each day 
## Purpose - Analysis of jobs run at HTCondor Batch System

## Updated by Teja Ram Seervi on 20/04/2018

import json, csv, pickle
import getopt, sys, os, logging
# pyspark liberary
from pyspark import SparkConf, SparkContext #HiveContext
from pyspark.sql import SQLContext

## list of fields to be extracted from the condor log
cols = 'JobPid, LastRemoteHost, User, JobStartDate, JobDuration, ExitCode, ExitStatus, ResidentSetSize, RequestMemory,\
 JobUniverse, RequestCpus, RemoteWallClockTime,  RemoteSysCpu, RemoteUserCpu,x509UserProxyVOName,MATCH_HEPSPEC,MATCH_TotalCpus'
local_dir = '/eos/project/i/it-awg/tseervi/condor/'
hdfs_dir = 'hdfs://analytix/user/tseervi/condor/'

def copy_from_hdfs_to_local_files(day_from, day_to):
	'''
	copy from hdfs to local file
	'''
 
	for d in range(day_from, day_to+1):
		# Condor Log File Name Construction
		dd = '{:02d}'.format(d)
		extracted_hdfs_dir = 'extracted/'+ yyyy + mm + dd +'/' 

		# Merge all log data files of signle day 
		# from extracted directory into a single file
		hdfs_src_dir = hdfs_dir + extracted_hdfs_dir
		local_dst_file = local_dir + yyyy + mm + dd + '.condorlog'
		cmd = 'hdfs dfs -getmerge ' + hdfs_src_dir +' '+ local_dst_file
		os.system(cmd)

		# Add header line to merged data
		local_header_file = local_dir + 'header.csv' 
		local_data_file = local_dst_file 
		local_csv_file = local_data_file+'.csv'
		cmd = 'cat '+ local_header_file + ' ' +local_data_file + ' > ' + local_csv_file
		os.system(cmd)

		# Remove data file without header file
		cmd = 'rm '+ local_data_file
		os.system(cmd)

def to_csv_line(data):
	''' 
	Returns comma separated line from data record 
	'''
	return ','.join(str(d) for d in data)


def extract_log(sqlContext, input_dir, output_dir):
	'''
	Extract logs from input_dir and saves them in output_dir(text format)
	'''
	logging.critical('==================== Extarcting log from: ' + input_dir +'=========================')

	# Read the JSON data of condor log into data frame df1
	# Reads a directory containing multiple JSON files 
	df1 = sqlContext.read.json(input_dir)
	
	# Register the dataframe
	df1.registerTempTable('df1') 

	# Extract the required fields in another data farme df2
	df2 = sqlContext.sql('select '+cols+' from df1')

	# Convert the map to list and save to local file
	maps = df2.map(to_csv_line)

	# Save the df2 in csv format in hdfs
	maps.saveAsTextFile(output_dir)

	logging.critical('==================== Extarcted log from: ' + input_dir +'=========================')

def usage():
	''' Displays how to run this program '''

	print(' --------------------------------------------------------------------------------------------------')
	print('spark-submit .... DataExtractDayWise.py --year=yyyy --month=m --from=day_from  --to=day_to')
	print('example $spark-submit --master local condor_log_extract_day_wise.py --year=2018 --month=4 --from=1 --to=30')
	print("example $spark-submit condor_log_extract_day_wise.py --year=2018 --month=4 --from=1 --to=30")
	print(' --------------------------------------------------------------------------------------------------')
	sys.exit(' ')

if __name__ == '__main__':
	logging.basicConfig(level=logging.WARNING, format='%(asctime)s: %(message)s', datefmt='%H:%M:%S')

	# parse args
	try:
		opts, args = getopt.getopt(sys.argv[1:],'h:ymft', ['help','year=','month=','from=','to='])
	except getopt.GetoptError:
		usage()

	# set parameters
	year = 0
	month = 0
	day_from = 0
	day_to = 0
	for o, a in opts:
		if o in ('-h', '--help'):
			usage()
		if o in ('-y', '--year'):
			year = int(a)
		if o in ('-m', '--month'):
			month = int(a)
		if o in ('-f', '--from'):
			day_from = int(a)
		if o in ('-t', '--to'):
			day_to = int(a)

	# Check if argument provided by user correct or not
	if year < 2015 or year > 2018:
		usage()
	if month < 1 or month > 12:
		usage()
	if day_from > day_to or day_from < 1 or day_to > 31:
		usage()

	## creteate spark and sql context
	conf = SparkConf()
	conf.setAppName('DataExtractDayWise')
	sc = SparkContext(conf = conf)
	sqlContext = SQLContext(sc)
	logging.critical('Created SparkContext, SQLContext.')

	# Formating year and month
	yyyy = '{:04d}'.format(year)
	mm = '{:02d}'.format(month)

	# Extract log for each day
	for d in range(day_from, day_to+1):
		dd = '{:02d}'.format(d)
		if year < 2018:
			input_dir = 'hdfs:///user/accservice/batch/condor/' + yyyy + '/' + mm + '/' + dd + '/*'
		else: # Log Data from Jan 2018 resides in cs3.cern.ch i.e. S3 server 
			input_dir = hdfs_dir + yyyy + '/' + mm + '/' + dd + '/'

		output_dir = hdfs_dir + 'extracted/' + yyyy + mm + dd + '/'
 
		logging.critical('input_dir: ' + input_dir)
		logging.critical('output_dir: ' + output_dir)
		extract_log(sqlContext, input_dir, output_dir)

	# Copy the extracted csv log data to local mahcine files
	copy_from_hdfs_to_local_files(day_from, day_to)
